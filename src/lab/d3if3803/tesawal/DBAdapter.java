/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lab.d3if3803.tesawal;
    
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import lab.d3if3803.tesawal.model.Job;

/**
 *
 * @author Agi Maulana
 */
public class DBAdapter {
    
    public ArrayList<Job> getJobs(){
        String query = "select * from job";
        ArrayList<Job> jobs = new ArrayList<Job>();
                
        try{
            Database db = new Database();
            db.Open();
            Statement stt = db.getConnection().createStatement();
            ResultSet rs = stt.executeQuery(query);
            while(rs.next()){
                Job job = new Job();
                job.setId(rs.getInt("id_job"));
                job.setName(rs.getString("job_name"));
                
                jobs.add(job);
            }
            db.close();    
        }catch(SQLException e){
            System.err.println("Error : " + e.getMessage());
        }
        
        return jobs;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lab.d3if3803.tesawal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Agi Maulana
 */
public class Database {
    private Connection connection;
    private final String DB_NAME = "tesawal";
    private final String DB_USERNAME = "root";
    private final String DB_PASSWORD = "admin";
    private final String DB_URL = "jdbc:mysql://localhost/"+DB_NAME;
    
    public void Open(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (ClassNotFoundException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (SQLException ex) {
            System.err.println("Error : " + ex.getMessage());
        }
    }
    
    public void close(){
        try{
            connection.close();
        }catch(SQLException ex){
            System.err.println("Error : " + ex.getMessage());
        }
    }
    
    public Connection getConnection(){
        return connection;
    }
}
